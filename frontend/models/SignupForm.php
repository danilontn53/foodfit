<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $cpf;
    public $telefone;
    public $dat_nascimento;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Este nome de usuário já está sendo utilizado.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            [['dat_nascimento'], 'safe'],

            ['cpf', 'filter', 'filter' => 'trim'],
            ['cpf', 'required', 'message' => '"{attribute}" não pode ficar em branco.'],
            ['cpf', 'filter', 'filter' => function($value) {  return str_replace(['.', '-'], '' , $value);}],
            ['cpf', 'unique', 'targetClass' => '\common\models\User', 'message' => 'CPF já cadastrado para outro usuário'],
            ['cpf', 'string', 'min' => 11, 'max' => 14],

            ['telefone', 'filter', 'filter' => 'trim'],
            ['telefone', 'string', 'min' => 4, 'max' => 15],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Este endereço de E-mail já está sendo utilizado.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Nome',
            'email' => 'E-mail',
            'password' => 'Senha',
            'cpf' => 'CPF',
            'telefone' => 'Telefone',
            'dat_nascimento' => 'Data de Nascimento',
        ];
    }


    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->cpf = $this->cpf;
        $user->telefone = $this->telefone;
        $user->dat_nascimento = $this->dat_nascimento;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
}
