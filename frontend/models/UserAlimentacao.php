<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_alimentacao".
 *
 * @property int $id_user_alimentacao
 * @property int $id_tipo_alimento
 * @property int $id
 * @property string $id_alimento
 * @property int $idn_meta
 * @property string $dat_refeicao
 *
 * @property Alimento $alimento
 * @property TipoAlimento $tipoAlimento
 */
class UserAlimentacao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_alimentacao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tipo_alimento', 'id_alimento', 'idn_meta', 'dat_refeicao', 'id'], 'required'],
            [['id_tipo_alimento', 'idn_meta'], 'default', 'value' => null],
            [['id_tipo_alimento', 'idn_meta'], 'integer'],
            [['dat_refeicao'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user_alimentacao' => 'Id User Alimentacao',
            'id_tipo_alimento' => 'Tipo Refeição',
            'id_alimento' => 'Alimento(s)',
            'idn_meta' => 'Meta',
            'dat_refeicao' => 'Data da Refeição',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlimento()
    {
        return $this->hasOne(Alimento::className(), ['id_alimento' => 'id_alimento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoAlimento()
    {
        return $this->hasOne(TipoAlimento::className(), ['id_tipo_alimento' => 'id_tipo_alimento']);
    }
}
