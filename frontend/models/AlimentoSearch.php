<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Alimento;

/**
 * AlimentoSearch represents the model behind the search form about `app\models\Alimento`.
 */
class AlimentoSearch extends Alimento
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_alimento', 'id_tipo_alimento'], 'integer'],
            [['dsc_alimento', 'porcao_quantidade'], 'safe'],
            [['caloria', 'carboidrato', 'proteina', 'lipidio'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Alimento::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_alimento' => $this->id_alimento,
            'id_tipo_alimento' => $this->id_tipo_alimento,
            'caloria' => $this->caloria,
            'carboidrato' => $this->carboidrato,
            'proteina' => $this->proteina,
            'lipidio' => $this->lipidio,
        ]);

        $query->andFilterWhere(['like', 'dsc_alimento', $this->dsc_alimento])
            ->andFilterWhere(['like', 'porcao_quantidade', $this->porcao_quantidade]);

        return $dataProvider;
    }
}
