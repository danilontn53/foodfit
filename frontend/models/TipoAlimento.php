<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_alimento".
 *
 * @property integer $id_tipo_alimento
 * @property string $dsc_tipo_alimento
 *
 * @property Alimento[] $alimentos
 */
class TipoAlimento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_alimento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dsc_tipo_alimento'], 'required'],
            [['dsc_tipo_alimento'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tipo_alimento' => 'Id Tipo Alimento',
            'dsc_tipo_alimento' => 'Dsc Tipo Alimento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlimentos()
    {
        return $this->hasMany(Alimento::className(), ['id_tipo_alimento' => 'id_tipo_alimento']);
    }
}
