<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_meta".
 *
 * @property int $id_user_meta
 * @property int $idn_meta
 * @property int $id
 * @property int $peso_meta
 *
 * @property Meta $nMeta
 */
class UserMeta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_meta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idn_meta'], 'required'],
            [['idn_meta'], 'default', 'value' => null],
            [['idn_meta', 'id'], 'integer'],
            [['idn_meta', 'id'], 'exist', 'skipOnError' => true, 'targetClass' => Meta::className(), 'targetAttribute' => ['idn_meta' => 'idn_meta']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user_meta' => 'Id User Meta',
            'idn_meta' => 'Idn Meta',
            'peso_meta' => 'Meta de Peso',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMeta()
    {
        return $this->hasOne(Meta::className(), ['idn_meta' => 'idn_meta']);
    }
}
