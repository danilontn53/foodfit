<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_peso".
 *
 * @property int $id_user_peso
 * @property int $id
 * @property string $peso_kg
 * @property string $dat_pesagem
 *
 * @property User $id0
 */
class UserPeso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_peso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'peso_kg', 'dat_pesagem'], 'required'],
            [['id'], 'default', 'value' => null],
            [['id'], 'integer'],
            [['peso_kg'], 'number'],
            [['dat_pesagem'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user_peso' => 'Id User Peso',
            'id' => 'ID',
            'peso_kg' => 'Peso Kg',
            'dat_pesagem' => 'Data da Pesagem',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
}
