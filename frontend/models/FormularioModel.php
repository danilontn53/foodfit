<?php
/**
 * Created by PhpStorm.
 * User: Danilo
 * Date: 28/05/2018
 * Time: 16:15
 */

namespace frontend\models;

use yii\base\Model;

class FormularioModel extends Model
{
    public $dsc_nome;
    public $email;
    public $dat_nascimento;
    public $idn_meta;

    public function rules(){
        return [
            [['dsc_nome', 'email', 'dat_nascimento', 'idn_meta'], 'required'],
            [['email'], 'email'],
            [['dat_nascimento'], 'date']
        ];
    }

    public function attributeLabels() {
        return [
          'dsc_nome' => 'Nome',
          'email' => 'E-mail',
          'dat_nascimento' => 'Data de Nascimento',
          'idn_meta' => 'Meta'
        ];
    }

}