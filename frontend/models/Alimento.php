<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alimento".
 *
 * @property integer $id_alimento
 * @property integer $id_tipo_alimento
 * @property string $dsc_alimento
 * @property string $porcao_quantidade
 * @property string $caloria
 * @property string $carboidrato
 * @property string $proteina
 * @property string $lipidio
 *
 * @property TipoAlimento $idTipoAlimento
 */
class Alimento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alimento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_tipo_alimento', 'dsc_alimento', 'porcao_quantidade', 'caloria', 'carboidrato', 'proteina', 'lipidio'], 'required'],
            [['id_tipo_alimento'], 'integer'],
            [['caloria', 'carboidrato', 'proteina', 'lipidio'], 'number'],
            [['dsc_alimento', 'porcao_quantidade'], 'string', 'max' => 100],
            [['id_tipo_alimento'], 'exist', 'skipOnError' => true, 'targetClass' => TipoAlimento::className(), 'targetAttribute' => ['id_tipo_alimento' => 'id_tipo_alimento']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_alimento' => 'Id Alimento',
            'id_tipo_alimento' => 'Id Tipo Alimento',
            'dsc_alimento' => 'Alimento',
            'porcao_quantidade' => 'Porção/Quantidade',
            'caloria' => 'Caloria',
            'carboidrato' => 'Carboidrato',
            'proteina' => 'Proteína',
            'lipidio' => 'Lipídio',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoAlimento()
    {
        return $this->hasOne(TipoAlimento::className(), ['id_tipo_alimento' => 'id_tipo_alimento']);
    }
}
