<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserAlimentacao;

/**
 * UserAlimentacaoSearch represents the model behind the search form of `app\models\UserAlimentacao`.
 */
class UserAlimentacaoSearch extends UserAlimentacao
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user_alimentacao', 'id_tipo_alimento', 'id_alimento', 'idn_meta'], 'integer'],
            [['dat_refeicao'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (!Yii::$app->user->isGuest) {
            $query = UserAlimentacao::find()->where('id =' . Yii::$app->user->identity->id);
        } else {
            $query = UserAlimentacao::find();
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_user_alimentacao' => $this->id_user_alimentacao,
            'id_tipo_alimento' => $this->id_tipo_alimento,
            'id_alimento' => $this->id_alimento,
            'idn_meta' => $this->idn_meta,
            'dat_refeicao' => $this->dat_refeicao,
        ]);

        return $dataProvider;
    }
}
