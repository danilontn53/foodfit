<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $role_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $cpf
 * @property string $telefone
 * @property string $dat_nascimento
 *
 * @property Avaliacao $avaliacao
 * @property UserAlimentacao[] $userAlimentacaos
 * @property UserMeta[] $userMetas
 * @property UserPeso[] $userPesos
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'role_id', 'cpf'], 'default', 'value' => null],
            [['status', 'role_id', 'cpf'], 'integer'],
            [['cpf'], 'required'],
            [['dat_nascimento'], 'safe'],
            [['username'], 'string', 'max' => 250],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['created_at', 'updated_at'], 'string', 'max' => 30],
            [['telefone'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'role_id' => 'Role ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'cpf' => 'Cpf',
            'telefone' => 'Telefone',
            'dat_nascimento' => 'Dat Nascimento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvaliacao()
    {
        return $this->hasOne(Avaliacao::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAlimentacaos()
    {
        return $this->hasMany(UserAlimentacao::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserMetas()
    {
        return $this->hasMany(UserMeta::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPesos()
    {
        return $this->hasMany(UserPeso::className(), ['id' => 'id']);
    }
}
