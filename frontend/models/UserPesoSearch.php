<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserPeso;

/**
 * UserPesoSearch represents the model behind the search form of `app\models\UserPeso`.
 */
class UserPesoSearch extends UserPeso
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user_peso', 'id'], 'integer'],
            [['peso_kg'], 'number'],
            [['dat_pesagem'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (!Yii::$app->user->isGuest) {
            $query = UserPeso::find()->where('id =' . Yii::$app->user->identity->id);
        } else {
            $query = UserPeso::find();
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_user_peso' => $this->id_user_peso,
            'id' => $this->id,
            'peso_kg' => $this->peso_kg,
            'dat_pesagem' => $this->dat_pesagem,
        ]);

        return $dataProvider;
    }
}
