<?php

namespace app\models;

use common\models\User;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "avaliacao".
 *
 * @property int $idn_avaliacao
 * @property int $id
 * @property integer $idn_meta
 * @property string $dat_nascimento
 * @property string $foto_antes
 * @property string $foto_depois
 * @property string $peso_atual
 * @property string $altura_atual
 * @property string $sexo
 * @property string $atividade_fisica_diaria
 * @property string $pratica_atividade_fisica
 * @property string $frequencia_atividade_fisica
 * @property int $qtd_alimentacao
 * @property string $principal_alimentacao
 * @property string $problema_saude
 *
 *
 * @property User $id0
 */
class Avaliacao extends \yii\db\ActiveRecord
{
    /**
     * @var UploadedFile
     */
    public $fotoClienteAntes;

    /**
     * @var UploadedFile
     */
    public $fotoClienteDepois;

    public $dsc_nome;
    public $email;
    public $idn_meta;
    public $dat_nascimento;
    public $peso_atual;
    public $altura_atual;
    public $sexo;
    public $atividade_fisica_diaria;
    public $pratica_atividade_fisica;
    public $frequencia_atividade_fisica;
    public $qtd_alimentacao;
    public $principal_alimentacao;
    public $problema_saude;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'avaliacao';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dsc_nome', 'email', 'dat_nascimento', 'idn_meta', 'peso_atual', 'altura_atual', 'sexo', 'fotoClienteAntes'], 'required'],
            [['email'], 'email'],
            [['fotoClienteAntes', 'fotoClienteDepois'], 'file', 'extensions' => 'jpg, png'],
            [['id', 'idn_meta'], 'required'],
            [['id', 'idn_meta', 'sexo'], 'integer'],
            [['peso_atual', 'altura_atual'], 'number'],
            [['dat_nascimento'], 'safe'],
            [['foto_antes', 'foto_depois'], 'string', 'max' => 100],
            [['atividade_fisica_diaria', 'pratica_atividade_fisica', 'frequencia_atividade_fisica', 'qtd_alimentacao', 'principal_alimentacao', 'problema_saude'], 'safe'],
            [['id'], 'unique'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dsc_nome' => 'Nome',
            'email' => 'E-mail',
            'idn_avaliacao' => 'Idn Avaliacao',
            'id' => 'ID',
            'dat_nascimento' => 'Data de Nascimento',
            'foto_antes' => 'Foto Antes',
            'foto_depois' => 'Foto Depois',
            'fotoClienteAntes' => 'Foto Atual',
            'fotoClienteDepois' => 'Foto após a conclusão da meta',
            'idn_meta' => 'Meta',
            'peso_atual' => 'Peso Atual',
            'altura_atual' => 'Altura Atual',
            'sexo' => 'Sexo',
            'atividade_fisica_diaria' => 'Como você descreveria suas atividades diárias normais?',
            'pratica_atividade_fisica' => 'Pratica alguma Atividade Física?',
            'frequencia_atividade_fisica' => 'Se pratica alguma Atividade Física, qual é a frequência? (Semanalmente)',
            'qtd_alimentacao' => 'Quantas vezes por dia você se alimenta?',
            'principal_alimentacao' => 'Que refeição considera ser a sua principal refeição do dia?',
            'problema_saude' => 'Possui algum tipo de Problema de Saúde? Se sim, especifique.',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
}
