$(function () {
    $(function () {
        $('#btnModalInserirRefeicao').click(function () {
            $('#modalInserirRefeicao').modal('show')
                .find('#modalContentInserirRefeicao')
                .load($(this).attr('value'));
        });
    });

    $('.btnModalAlterarRefeicao').click(function () {
        $('#modalAlterarRefeicao').modal('show')
            .find('#modalContentAlterarRefeicao')
            .load($(this).attr('value'));
    });

    $('#btnModalInserirPeso').click(function () {
        $('#modalInserirPeso').modal('show')
            .find('#modalContentInserirPeso')
            .load($(this).attr('value'));
    });

    $('.btnModalAlterarPeso').click(function () {
        $('#modalAlterarPeso').modal('show')
            .find('#modalContentAlterarPeso')
            .load($(this).attr('value'));
    });

    $("#menu a").click(function (event) {
        if ($(this).next('ul').length) {
            event.preventDefault();
            $(this).next().toggle('fast');
            $(this).children('i:last-child').toggleClass('fa-caret-down fa-caret-left');
        }
    });

});
