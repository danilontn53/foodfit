<?php

use kartik\select2\Select2;
use yii\bootstrap\Html;
use yii\web\JsExpression;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
$form = ActiveForm::begin(['action' => ['site/index']]);
$model = new \common\models\LoginForm();


?>

    <div class="row">

        <?= $form->field($model, 'username')->textarea(['rows' => 6, 'class'=> "form-control required"]) ?>

    </div>



