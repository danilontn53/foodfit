<?php
/**
 * Created by PhpStorm.
 * User: Danilo
 * Date: 21/05/2018
 * Time: 17:23
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

//centralizando a mensagem de sucesso de validação
$this->registerCss(".css {
    margin: 0 auto;
    margin-bottom: 15px;
}");

?>

<div class="text-center">
    <h2 class="css alert alert-success text-center" style="width: 60%">
        <span class="glyphicon glyphicon-ok-circle"></span>
        Dados Validados com Sucesso!
    </h2>
</div>

<div class="text-center">
    <?php $form = ActiveForm::begin() ?>
    <div class="row buttons">
        <?= Html::submitButton('Salvar Dados <span class="glyphicon glyphicon-floppy-save"></span>', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
