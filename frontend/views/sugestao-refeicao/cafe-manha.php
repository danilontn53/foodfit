<?php

$this->registerCss('
.box {
    max-width:975px;
	max-height:164px;
}');

use yii\helpers\Html;

?>

    <div>
        <?php
        echo Html::img('@web/imagens/cafe-manha.jpg', ['alt'=>'some', 'class'=>'box']);

        ?>
    </div>

<?php
echo "<h3>Opção 1 - Café da manhã saudável e light reforçado</h3>";
echo "<hr>";

echo "<b>Alimentos:</b><br><br>";

echo "<li><label style='margin-right: 8px;'>1 xícara de café com leite desnatado e adoçante;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>2 fatias de pão integral com grãos;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 fatia de queijo branco ou cottage;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 fatia de peito de peru;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 banana;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 copo pequeno de suco de laranja natural sem adoçar e sem coar.</i></label></li>";

echo "<h3>Opção 2 - Café da manhã saudável e light rápido</h3>";
echo "<hr>";

echo "<b>Alimentos:</b><br><br>";

echo "<li><label style='margin-right: 8px;'>Vitamina: 1 copo de leite desnatado, 1 colher de aveia, ½ maçã, ½ banana sem casca.</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 fatia de pão integral com grãos;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 1 fatia de queijo branco.</i></label></li>";

?>