<?php

use yii\helpers\Html;
?>
    <div>
        <?php
        echo Html::img('@web/imagens/almoco.jpg', ['alt'=>'some', 'class'=>'box']);
        ?>
    </div>

<?php
echo "<h3>Opção 1 - Arroz Integral Colorido</h3>";
echo "<hr>";

echo "<b>Ingredientes:</b><br><br>";

echo "<li><label style='margin-right: 8px;'>½ xícara de arroz integral;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>2 colheres de sopa de cenoura relada;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>2 colheres de sopa de brócolis esfarelado;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 colher de sopa de couve cortado em tirinhas finas;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 cebola picadinha;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>2 dentes de alho;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 pitada de sal e pimenta do reino;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 fio de azeite.</i></label></li><br>";

echo "<b>Modo de preparo:</b><br>";

echo "Essa é para colorir o seu almoço. Vamos testar? Em uma panela refogue a cebola e o alho com um fio de azeite de oliva. Adicione o arroz, mexa bastante, em seguida acrescente água fervente suficiente para cozinhar o arroz. Quando os grãos estiverem quase prontos ou “al dente” adicione os vegetais e misture bem. Se precisar coloque mais um pouquinho de água fervente. Espere 5 minutos e desligue. Pode saborear!";

echo "<h3>Opção 2 - Frango Grelhado Funcional</h3>";
echo "<hr>";

echo "<b>Ingredientes:</b><br><br>";

echo "<li><label style='margin-right: 8px;'>1 filé de peito de frango;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 cebola cortada em rodelas;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 colher de sopa de azeite;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 alho, pimenta do reino e sal a gosto.</i></label></li><br>";

echo "<b>Modo de preparo:</b><br>";

echo "Tempere o frango com sal, alho e pimenta do reino. Grelhe o frango até que ele fique dourado. Pode ser com grelha convencional ou numa frigideira antiaderente.";

?>