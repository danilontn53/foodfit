<?php

use yii\helpers\Html;
?>
    <div>
        <?php
        echo Html::img('@web/imagens/lanches.jpg', ['alt'=>'some', 'class'=>'box']);
        ?>
    </div>

<?php
echo "<h3>Opção 1 - Receita de tapioca com ricota</h3>";
echo "<hr>";

echo "<b>Ingredientes:</b><br><br>";

echo "<li><label style='margin-right: 8px;'>1 pacote de massa pronta de tapioca;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>5 colheres de sopa de ricota;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 colher de sopa de chia;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>3 colheres de sopa de mel.</i></label></li><br>";

echo "<b>Modo de preparo:</b><br>";

echo "Separe uma frigideira anti-aderente. Cubra com a goma de tapioca, espalhando bem pela superfície de forma uniforme. Espere firmar de um lado e começar a soltar da borda, então vire e deixe do outro lado. Coloque ricota amassada, chia e mel e dobre ao meio. Sirva.";

echo "<h3>Opção 2 - Receita de salada de frutas</h3>";
echo "<hr>";

echo "<b>Ingredientes:</b><br><br>";

echo "<li><label style='margin-right: 8px;'>1 pera picada;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>3 bananas médias picada;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 goiaba picada com casca;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 bergamota separada em gomos;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>12 morangos picados;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>2 copos de iogurte desnatado;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 xícara de granola sem açúcar;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 colher de sopa de sementes de linhaça;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>2 colheres de sopa de mel.</i></label></li><br>";

echo "<b>Modo de preparo:</b><br>";

echo "Faça o pré-preparo das frutas lavando e picando conforme instruções. Adicione as frutas em taças de vidro, acrescente o iogurte e finalize com granola por cima e um fio de mel. Sirva.";

?>