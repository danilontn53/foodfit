<?php

use yii\helpers\Html;
?>
    <div>
        <?php
        echo Html::img('@web/imagens/jantar.jpg', ['alt'=>'some', 'class'=>'box']);
        ?>
    </div>

<?php
echo "<h3>Opção 1 - Receita de almôndega de carne</h3>";
echo "<hr>";

echo "<b>Ingredientes:</b><br><br>";

echo "<li><label style='margin-right: 8px;'>500g de carne moída;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>2 colheres de sopa de óleo de coco;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 xícara de chá de cebola triturada;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>5 colheres de sopa de farinha de aveia;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>½ xícara de hortelã picada;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 ovo;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 sal a gosto;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>pimenta-do-reino a gosto.</i></label></li><br>";

echo "<b>Modo de preparo:</b><br>";

echo "Em um bowl misture a carne, a cebola, o sal, o ovo, o óleo de coco, a pimenta e o hortelã picada. Adicione a farinha de aveia aos poucos até dar o ponto de enrolar as almôndegas. Modele as almôndegas com as mãos e coloque em uma assadeira com papel manteiga. Leve ao forno para assar em temperatura de 180°C por 35 minutos. Sirva em seguida. Se quiser prepare um molho de tomate caseiro para acompanhar.";

echo "<h3>Opção 2 - Receita de salada de macarrão com frango</h3>";
echo "<hr>";

echo "<b>Ingredientes:</b><br><br>";

echo "<li><label style='margin-right: 8px;'>½ pacote de macarrão parafuso integral;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 peito de frango cozido e desfiado;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 dente de alho amassado;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 colher de sopa de azeite de oliva;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 colher de sopa de vinagre de maçã;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 tomate sem pele e sem sementes cortado em cubinhos;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>2 colheres de sopa de manjericão fresco picado;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>1 colher de sopa de sementes de linhaça;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>sal a gosto;</i></label></li>";
echo "<li><label style='margin-right: 8px;'>pimenta a gosto.</i></label></li><br>";

echo "<b>Modo de preparo:</b><br>";

echo "Cozinhe o frango em panela de pressão e depois escorra, desfie, tempere e reserve. Cozinhe o macarrão até ficar al dente, escorra e reserve em um recipiente até esfriar. Em uma saladeira coloque o alho amassado, o azeite, o vinagre de maçã, o tomate, o manjericão, o frango desfiado e adicione o macarrão já frio. Misture bem e tempere com sal e pimenta. Leve para gelar e na hora de servir adicione a linhaça e regue com um fio de azeite.";

?>