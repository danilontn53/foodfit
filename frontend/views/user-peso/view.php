<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserPeso */

$this->title = 'Meu peso referente ao dia' . " (" . date('d/m/Y', strtotime($model['dat_pesagem'])) . ")";
$this->params['breadcrumbs'][] = ['label' => 'Meus Pesos'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-peso-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="pull-left">
        <a href="javascript:history.back()" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Voltar</a>
    </div>

    <p class="text-right">
        <?= Html::a('<i class="glyphicon glyphicon-pencil" style="color: #fff"></i> Atualizar', ['update', 'id' => $model->id_user_peso, 'tela' => 'view'], ['class' => 'btn btn-primary']) ?>
        <?=  Html::a('<i class="glyphicon glyphicon-trash" style="color: #fff"></i> Apagar', ['delete', 'id' => $model->id_user_peso], [
            'class' => 'btn btn-danger btn_verm btn_azul',
            'data' => [
                'confirm' => 'Tem certeza que deseja excluir este Peso?',
                'method' => 'post',
            ],
        ]); ?>

    <div>
        <?= DetailView::widget([
            'model' => $model,
            'options' => ['class' => 'table table-striped table-bordered detail-view tab_perf_det'],
            'attributes' => [
                [
                    'attribute' => 'peso_kg',
                ],
                [
                    'attribute' => 'dat_pesagem',
                    'value' => function ($data) {
                        return date('d/m/Y', strtotime($data['dat_pesagem']));
                    }
                ]
            ],
        ]) ?>
    </div>


</div>
