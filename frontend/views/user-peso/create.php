<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserPeso */

$this->title = 'Inserir Peso';
$this->params['breadcrumbs'][] = ['label' => 'Meus Pesos'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-peso-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
