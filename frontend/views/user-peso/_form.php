<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserPeso */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-peso-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'peso_kg')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '99.99',
    ])?>

    <?= $form->field($model, 'dat_pesagem')->widget(DatePicker::className(), [
        'options' => ['placeholder' => '', 'id' => 'dp1',],
        'language' => 'pt',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'pluginOptions' => [
            'language' => 'pt',
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy',
            ]
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<i class="glyphicon glyphicon-floppy-save" style="color: #fff"></i> Inserir' : '<i class="glyphicon glyphicon-pencil" style="color: #fff"></i> Alterar', ['class' => $model->isNewRecord ? 'btn btn-primary btn_azul' : 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
