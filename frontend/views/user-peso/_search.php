<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\UserPesoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-peso-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_user_peso') ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'peso_kg') ?>

    <?= $form->field($model, 'dat_pesagem') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
