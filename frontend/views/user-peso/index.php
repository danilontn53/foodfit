<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\UserPesoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Meus Pesos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-peso-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="pull-left">
        <a href="javascript:history.back()" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i>
            Voltar</a>
    </div>

    <p class="text-right">
        <?= Html::button('<i class="glyphicon glyphicon-floppy-save" style="color: #fff"></i> Inserir Peso', [
            'value' => Url::to(['create']),
            'class' => 'btn btn-primary',
            'id' => 'btnModalInserirPeso',
        ]) ?>
    </p>

    <?php
    Modal::begin([
        'header' => '<h4>Inserir Peso</h4>',
        'id'     => 'modalInserirPeso',
        'size'   => 'model-lg',
    ]);

    echo "<div id='modalContentInserirPeso'></div>";

    Modal::end();
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'emptyText' => 'Nenhum Peso encontrado.',
        'export' => false,
        'tableOptions' => ['class' => 'tab_perf'],
        'columns' => [
            [
                'attribute' => 'peso_kg',
            ],
            [
                'attribute' => 'dat_pesagem',
                'value' => function ($data) {
                    return date('d/m/Y', strtotime($data['dat_pesagem']));
                }
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'headerOptions' => ['class' => 'col_acao'],
                'contentOptions' => ['class' => 'col_acao_int'],
                'header' => 'Ações',
                'viewOptions' => ['title' => 'Visualizar'],
                'deleteOptions' => ['label' => "<i class='glyphicon glyphicon-remove'></i>",
                    'title' => 'Excluir',
                    'msg' => 'Tem certeza que deseja excluir este Peso?'
                ],
                'buttons' => [
                    //update button
                    'update' => function ($url) {
                        return Html::button(null, [
                            'value' => Url::to($url),
                            'class' => 'btnModalAlterarPeso glyphicon glyphicon-pencil',
                            'style' => 'background: transparent; border: 0; outline: none; -webkit-appearance: none; color: #337AB7;',
                            'title' => 'Editar',
                        ]);
                    },
                ],
            ],
        ],
        'krajeeDialogSettings' => [
            'options' => [
                'title' => 'Aviso',
                'btnOKLabel' => 'Sim',
                'btnCancelLabel' => 'Não',
            ],
        ],
        'pager' => [
            'options' => [
                'class' => 'pagination paginacao_sgi',
            ],
        ],
    ]); ?>


    <?php
    Modal::begin([
        'header'=> '<h4> Alterar Peso</h4>',
        'id' => 'modalAlterarPeso',
    ]);

    echo "<div id='modalContentAlterarPeso'></div>";
    Modal::end();

    ?>
</div>

<?php $this->registerJs($this->render('@frontend/web/js/tabelas.js')); ?>
