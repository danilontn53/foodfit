<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserPeso */

$this->title = 'Atualizar Meu peso referente ao dia' . " (" . date('d/m/Y', strtotime($model['dat_pesagem'])) . ")";
$this->params['breadcrumbs'][] = ['label' => 'Meus Pesos'];
$this->params['breadcrumbs'][] = ['label' => $model->id_user_peso, 'url' => ['view', 'id' => $model->id_user_peso]];
$this->params['breadcrumbs'][] = 'Atualizar';
?>
<div class="user-peso-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
