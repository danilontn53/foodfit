<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\Avaliacao;
use frontend\assets\AppAsset;
use kartik\nav\NavX;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\bootstrap\NavBar;
use yii\helpers\Url;

AppAsset::register($this);

$this->registerCss("
.login .modal-dialog {
    width: 25%;
        border-radius: 6px;
}");

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!--Cabeçalho-->
<header class="bloco_cabecalho">
    <div class="container">
        <h1 class="bloco_nome_orgao">
            <a href="/foodfit_oficial/frontend/web/">
                FOODFIT
                <span class="subordinacao_orgao">Controle de Alimentação</span>
            </a>
        </h1>
        <a href="#" class="logo_subordinação">
            <span class="imagem_logo"></span>
        </a>
    </div>
</header>

<div class="wrap">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php
    NavBar::begin([

        'options' => [
            'class' => 'navbar bloco_menu_prim navbar-default',
        ],
    ]);

    $menuItems = [

        ['label' => 'Início', 'url' => ['/site/index']],
        ['label' => 'Contato', 'url' => ['/site/contact']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Cadastro', 'url' => ['/site/signup']];
        $menuItems[] = '<font style="color: white; font-size: 13.5px" ">'.Html::button('Login <span class="glyphicon glyphicon-log-in"></span>', [
            'value' => Url::to(['login']),
            'style' => 'background: transparent !important;  border-color: transparent !important;',
            'id' => 'btnModalRealizarLogin',
        ]).'</font>';
    } else {

        $avaliacao_true = Avaliacao::find()->where(['id' => Yii::$app->user->identity->id])->one();

        if ($avaliacao_true == null) {
            $menuItems[] = ['label' => 'Realizar Avaliação', 'url' => ['/site/avaliacao']];
        }
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Sair <span class="glyphicon glyphicon-log-out"></span>',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';

    }

    if (!Yii::$app->user->isGuest) echo "<span class='nome_usuario_header'>
    <i class='fa fa-user-circle' style='font-size: 15px'></i>
         Usuário(a): " .Yii::$app->user->identity->username. "</span>";
    echo NavX::widget([
        'options' => ['class' => 'menu_prim_itens'],
        'encodeLabels' => false,
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="login">
        <?php
        Modal::begin([
            'header' => '<h4>Realizar Login</h4>',
            'id' => 'modalRealizarLogin',
        ]);

        echo $this->render('..\site\modal_login');
        Modal::end();
        ?>
    </div>

    <nav class="container bloco_breadcrumb">
        <label style="">você está aqui:</label>
        <?=
        \yii\widgets\Breadcrumbs::widget([
            'homeLink' => false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
        ?>
    </nav>

    <div class="container">
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; FoodFit <?= date('Y') ?></p>

        <p class="pull-right"><?= 'Sistemas de Informação'?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php $this->registerJs($this->render('@frontend/web/js/tela-login/tela-login.js')); ?>

