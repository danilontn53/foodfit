<?php
/* @var $this yii\web\View */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Avaliação';
$this->params['breadcrumbs'][] = $this->title;

?>
<!-- Flashes -->
<?php foreach (Yii::$app->session->getAllFlashes() as $message):; ?>
    <?php
    echo \kartik\widgets\Growl::widget([
        'type' => (!empty($message['type'])) ? $message['type'] : 'danger',
        'title' => (!empty($message['title'])) ? Html::encode($message['title']) : '',
        'icon' => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
        'body' => (!empty($message['message'])) ? $message['message'] : 'Message Not Set!',
        'showSeparator' => true,
        'delay' => 1, //This delay is how long before the message shows
        'pluginOptions' => [
            'delay' => (!empty($message['duration'])) ? $message['duration'] : 3000, //This delay is how long the message shows for
            'placement' => [
                'from' => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
                'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
            ]
        ]
    ]);
    ?>
<?php endforeach; ?>

<div class="site-about">

    <?php $form = ActiveForm::begin() ?>

    <?php
    $wizard_config = [
        'id' => 'stepwizard',
        'steps' => [
            1 => [
                'title' => 'Informações Pessoais',
                'label' => 'Informações Pessoais',
                'icon' => 'glyphicon glyphicon-user',
                'content' => $this->render('..\formularios\dados-pessoais', [
                        'formulario' => $formulario,
                        'form' => $form
                ]),
                'buttons' => [
                    'next' => [
                        'title' => 'Prosseguir',
                        'options' => [
                            'class' => 'btn btn-primary btn_azul',
                            'style' => 'margin-top: 7px;'
                        ],
                    ],
                ],
            ],
            2 => [
                'title' => 'Informações sobre Alimentação e Saúde',
                'icon' => 'glyphicon glyphicon-apple',
                'content' => $this->render('..\formularios\alimentacao-saude', [
                    'formulario' => $formulario,
                    'form' => $form
                ]),
                'skippable' => true,

            ],
            3 => [
                'title' => 'Informações sobre Atividades Físicas',
                'icon' => 'glyphicon glyphicon-heart',
                'content' => $this->render('..\formularios\atividades-fisicas', [
                    'formulario' => $formulario,
                    'form' => $form
                ]),
                'skippable' => true,

            ],
            4 => [
                'title' => 'Selecionar uma Meta',
                'icon' => 'glyphicon glyphicon-random',
                'content' =>  $this->render('..\formularios\meta', [
                    'formulario' => $formulario,
                    'form' => $form
                ]),
                'buttons' => [
                    'next' => [
                        'title' => 'Prosseguir',
                        'options' => [
                            'class' => 'btn btn-primary btn_azul',
                            'style' => 'margin-top: 7px;'
                        ],
                    ],
                ],
            ],
        ],
        'complete_content' => $this->render('..\forms\validacao'), // Optional final screen

        'start_step' => 1, // Optional, start with a specific step
    ];
    ?>

    <?= \drsdre\wizardwidget\WizardWidget::widget($wizard_config); ?>
    <?php ActiveForm::end() ?>


</div>
