<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

$this->title = 'Realizar novo cadastro';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Por favor, preencha os seguintes campos para realizar um novo cadastro:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'dat_nascimento')->widget(DatePicker::className(), [
                'options' => ['placeholder' => '', 'id' => 'dp1',],
                'language' => 'pt',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'language' => 'pt',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd/mm/yyyy',
                    ]
                ]
            ]) ?>

            <?= $form->field($model, 'cpf')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '999.999.999-99',
            ])
            ?>

            <?= $form->field($model, 'telefone')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => ['(99) 9999-9999', '(99) 99999-9999'],
            ])
            ?>

            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Cadastrar <span class="glyphicon glyphicon-floppy-saved"></span>',
                    [
                        'class' => 'btn btn-primary',
                        'name' => 'login-button',
                        'id' => 'load',
                        'data-loading-text' => "<i class='fa fa-spinner fa-spin '></i> Processando"

                    ]) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php $this->registerJs($this->render('@frontend/web/js/tela-login/buttons.js')); ?>

