<?php

/* @var $this yii\web\View */

use app\models\Avaliacao;
use app\models\Meta;
use app\models\UserAlimentacao;
use common\components\Utils;
use dosamigos\highcharts\HighCharts;
use kartik\helpers\Html;
use yii\bootstrap\Modal;
use yii\db\Query;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'FoodFit';
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss("
.grafico {
    margin-left: 285px;
    margin-top: -297px;
}

.col-lg-5 {
    width: 41.66666667%;
    margin-top: -445px;
    margin-left: 665px;

}
");


?>

<?php foreach (Yii::$app->session->getAllFlashes() as $message):; ?>
    <?php
    echo \kartik\widgets\Growl::widget([
        'type' => (!empty($message['type'])) ? $message['type'] : 'danger',
        'title' => (!empty($message['title'])) ? Html::encode($message['title']) : '',
        'icon' => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
        'body' => (!empty($message['message'])) ? $message['message'] : 'Message Not Set!',
        'showSeparator' => true,
        'delay' => 1, //This delay is how long before the message shows
        'pluginOptions' => [
            'delay' => (!empty($message['duration'])) ? $message['duration'] : 3000, //This delay is how long the message shows for
            'placement' => [
                'from' => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
                'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
            ]
        ]
    ]);
    ?>
<?php endforeach; ?>

<div class="site-index">
    <!--    <div class="jumbotron">-->
    <div style="text-align: center;">
        <?php if (Yii::$app->user->isGuest): ?>
            <h1>Sejam bem-vindos!</h1>

            <p class="lead">Experimente esse novo conceito em alimentação saudável.</p>

            <?php $form = ActiveForm::begin(['action' => ['site/signup']]) ?>
            <div class="row buttons">
                <?= Html::submitButton('Realizar cadastro', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end() ?>
        <?php endif; ?>

    </div>

    <div class="body-content">
        <?php if (Yii::$app->user->isGuest): ?>
            <div class="row">
                <div class="col-lg-4">
                    <h2>O que é uma alimentação saudável?</h2>

                    <p>A alimentação saudável é a ingestão de, principalmente, alimentos naturais em nossas refeições
                        diárias.</p>

                    <?php
                    Modal::begin([
                        'header' => '<font size="5" style="text-transform: uppercase;">O que é uma alimentação saudável?</font>',
                        'toggleButton' => ['label' => 'Continue lendo... &raquo;',
                            'class' => 'btn btn-default'],
                    ]);
                    echo 'A alimentação saudável é a ingestão de, principalmente, alimentos naturais em nossas refeições diárias. Nessa é necessário a absorção de açúcares, carnes, ovos, hortaliças, frutas, legumes, leite, óleos, massas, raízes e tubérculos. A quantidade necessária para a alimentação saudável varia para cada organismo já que é levado em consideração a altura, a idade, o peso, e a saúde de cada indivíduo, além das atividades físicas praticadas pelo mesmo.';
                    Modal::end();
                    ?>
                </div>
                <div class="col-lg-4">
                    <h2>Por que ter uma alimentação saudável?</h2>

                    <p>A alimentação saudável é o principal responsável para o organismo ter um bom desempenho. O nosso
                        corpo precisa diariamente de vitaminas e minerais para funcionar perfeitamente.</p>

                    <?php
                    Modal::begin([
                        'header' => '<font size="5" style="text-transform: uppercase;">Por que ter uma alimentação saudável?</font>',
                        'toggleButton' => ['label' => 'Continue lendo... &raquo;',
                            'class' => 'btn btn-default'],
                    ]);
                    echo 'A alimentação saudável é o principal responsável para o organismo ter um bom desempenho. O nosso corpo precisa diariamente de vitaminas e minerais para funcionar perfeitamente. Um cardápio equilibrado traz diversos benefícios, tanto para o aspecto físico, quanto para o mental.
<br>Além de ter mais disposição e energia, uma pessoa com a alimentação saudável é menos suscetível a ter algumas doenças, como: obesidade, câncer, artrite, anemia, diabetes e hipertensão.
<br>Uma boa alimentação pode parecer difícil de se manter, mas tudo é uma questão de hábito e persistência. ';
                    Modal::end();
                    ?>
                </div>
                <div class="col-lg-4">
                    <h2>Como ter uma alimentação saudável?</h2>

                    <p>Ter uma alimentação saudável é imprescindível para que o corpo funcione corretamente e você tenha
                        energia. Assim, simplificando, você precisa incluir no cardápio todos os nutrientes necessários
                        para o organismo realizar suas funções. </p>

                    <?php
                    Modal::begin([
                        'header' => '<font size="5" style="text-transform: uppercase;">Como ter uma alimentação saudável?</font>',
                        'toggleButton' => ['label' => 'Continue lendo... &raquo;',
                            'class' => 'btn btn-default'],
                    ]);
                    echo 'Ter uma alimentação saudável é imprescindível para que o corpo funcione corretamente e você tenha energia. Assim, simplificando, você precisa incluir no cardápio todos os nutrientes necessários para o organismo realizar suas funções. 
                <br>O excesso de informação a respeito de dietas da moda pode impedir as pessoas de obterem todos os nutrientes necessários para uma saúde em dia. Mas o primeiro passo para uma alimentação saudável é simplesmente evitar produtos industrializados, tais como: congelados, embutidos e processados.
                <br><br>Ficou curioso? <A href="http://localhost/foodfit_oficial/frontend/web/site/signup" title="Realizar Cadastro">Faça já o seu cadastro</A> na <b><i>FoodFit</i></b> e venha participar desse inovador conceito em Alimentação Saudável!';
                    Modal::end();
                    ?>
                </div>
            </div>
        <?php endif; ?>

        <?php
        if (!Yii::$app->user->isGuest) {
            $avaliacao_true = Avaliacao::find()->where(['id' => Yii::$app->user->identity->id])->one();
        } else {
            $avaliacao_true = null;
        }
        ?>

        <?php if (!Yii::$app->user->isGuest && $avaliacao_true != null): ?>
        <!-- menu vertical -->
        <nav id="menu" class="left show">
            <ul>
                <li><a href="http://localhost/foodfit_oficial/frontend/web/site/index" class="active"><i
                                class="glyphicon glyphicon-user"></i>Meus Dados</a></li>
                <li><a href="#"><i class="fa fa-info-circle"></i>Metas</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-cutlery"></i>Alimentação</a></li>
                <li><a href="http://localhost/foodfit_oficial/frontend/web/site/sugestao-refeicao">Sugestão de
                        Alimentação</a></li>
                <li><a href="http://localhost/foodfit_oficial/frontend/web/user-alimentacao/index">Informar
                        Refeições</a></li>
                <li><a href="http://localhost/foodfit_oficial/frontend/web/user-peso">Informar Peso</a></li>
                <li><a href="http://localhost/foodfit_oficial/frontend/web/progresso"><i class="fa fa-line-chart"></i>Progresso</a>
                </li>
            </ul>
        </nav>
        <cs

        <div class="col-lg-4 grafico"
        <?php

        $user = (new Query())
            ->select("*")
            ->from("avaliacao")
            ->where('id = ' . Yii::$app->user->identity->id)
            ->one();

        $peso = (new Query())
            ->select("*")
            ->from("user_peso")
            ->where('id = ' . Yii::$app->user->identity->id)
            ->one();

        $user['sexo'] == 1 ? $sexo = 'Feminino' : $sexo = 'Masculino';


        echo "<h5><label style='margin-right: 8px;'>" . "Nome: </label>" . Yii::$app->user->identity->username . "</h5>";
        echo "<h5><label style='margin-right: 8px;'>" . "Sexo: </label>" . $sexo . "</h5>";
        echo "<h5><label style='margin-right: 8px;'>" . "Data de Nascimento: </label>" . date('d/m/Y', strtotime(Yii::$app->user->identity->dat_nascimento)) . "</h5>";
        echo "<h5><label style='margin-right: 8px;'>" . "Peso Inicial: </label>" . $user['peso_atual'] . "</h5>";

        echo "<h5><label style='margin-right: 8px;'>" . "Meta: </label>" . Meta::find($model->idn_meta)->one()->dsc_meta . "</h5>";
        ?>
        <?php

        $kcal_indicadas = (new Query())
            ->select("*")
            ->from("user_meta")
            ->where('id = ' . Yii::$app->user->identity->id)
            ->one();

        $kcal_indicadas = (float)$kcal_indicadas['peso_meta'];

        $this->registerCss('
        .box {
            max-width:975px;
            max-height:164px;
        }');

        echo Html::img('@web/imagens/perfil.jpg', ['alt' => 'some', 'class' => 'box']);

        echo "<hr>";
        echo "<h5 class='borda' style='margin-right: 8px;'>" . "Parabéns! Seu perfil está completo. <br><br>De acordo com as suas respostas, para um bom progresso e sucesso com a Meta escolhida, o seu Consumo Diário de Calorias deve ser: <label>" . $kcal_indicadas . " Kcal</label></h5>";

        $dataFonteOGU[] = ['name' => 'teste', 'y' => 50];
        $dataFonteOGU[] = ['name' => 'teste 2', 'y' => 50];
        ?>

    </div>
    <div class="col-lg-5">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title"><?='Minhas calorias (kcal) diárias ('. $kcal_indicadas . ' Kcal sugeridas diariamente)'?></h2>
            </div>
            <div class="panel-body">
                <?php echo HighCharts::widget([
                    'clientOptions' => [
                        'width' => '100%',
                        'chart' => [
                            'plotBackgroundColor' => null,
                            'plotBorderWidth' => null,
                            'plotShadow' => false,
                            'type' => 'pie',
                            'lang' => [
                                'decimalPoint' => '.',
                                'thousandsSep' => ',',
                            ],
                        ],
                        'title' => [
                            'text' => 'Minhas calorias (kcal) diárias'
                        ],
                        'tooltip' => [
                            'pointFormat' => '{series.name}: <b> {point.y} (Kcal)</b>'
                        ],
                        'plotOptions' => [
                            'pie' => [
                                'allowPointSelect' => true,
                                'cursor' => 'pointer',
                                'dataLabels' => [
                                    'format' => '{point.y} (Kcal)',
                                    'enabled' => true,
                                    'connectorColor' => 'silver',
                                    'distance' => -65,
                                    'rotation' => 0
                                ],
                                'showInLegend' => true,
                            ]
                        ],
                        'series' => [[
                            'name' => 'Total',
                            'colorByPoint' => true,
                            'data' => $dateObjeto,
                        ]],
                        'credits' => [
                            'enabled' => false
                        ],
                        'lang' => [
                            'months' => ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                            'shortMonths' => ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                            'weekdays' => ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                            'loading' => ['Atualizando o gráfico...aguarde'],
                            'contextButtonTitle' => 'Exportar Gráfico',
                            'decimalPoint' => ',',
                            'thousandsSep' => '.',
                            'downloadJPEG' => 'Download em JPEG',
                            'downloadPDF' => 'Download em PDF',
                            'downloadPNG' => 'Download em PNG',
                            'downloadSVG' => 'Download vetor SVG',
                            'printChart' => 'Imprimir Gráfico',
                            'rangeSelectorFrom' => 'De',
                            'rangeSelectorTo' => 'Para',
                            'rangeSelectorZoom' => 'Zoom',
                            'resetZoom' => 'Limpar Zoom',
                            'resetZoomTitle' => 'Voltar Zoom para nível 1:1',
                        ],
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>

    <?php endif; ?>
</div>
</div>

<?php $this->registerJs($this->render('@frontend/web/js/tabelas.js')); ?>
