<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;

$this->title = 'Sugestão de Alimentação';
$this->params['breadcrumbs'][] = ['label' => 'Meus Dados'];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="pull-left">
    <a href="javascript:history.back()" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i>
        Voltar</a>
</div>

<?php

$items = [

    [
        'label' => 'Café da Manhã',
        'content' => $this->render('../sugestao-refeicao/cafe-manha'),
        'active' => true
    ],
    [
        'label' => 'Almoço',
        'content' => $this->render('../sugestao-refeicao/almoco'),
    ],
    [
        'label' => 'Jantar',
        'content' => $this->render('../sugestao-refeicao/jantar'),
    ],
    [
        'label' => 'Lanches',
        'content' => $this->render('../sugestao-refeicao/lanches'),

    ],
]; ?>

<div>
    <br>
    <br>
    <?= TabsX::widget([
        'items' => $items,
        'position' => TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'bordered' => true,
    ]); ?>

    <br>
    <br>
    <br>
</div>

