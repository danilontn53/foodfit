<?php
/**
 * Created by PhpStorm.
 * User: Danilo
 * Date: 28/05/2018
 * Time: 19:48
 */


use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;


$result = (new \yii\db\Query())
    ->select('*')
    ->from('avaliacao')
    ->where(['id' => Yii::$app->user->identity->id]);

$dataProvider = new ActiveDataProvider([
    'query' => $result,
]);



\common\components\Utils::loga($dataProvider);
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'dat_nascimento',
        // ...
    ],
]) ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'summary' => '',
    'emptyText' => 'Nenhum perfil encontrado.',
    'export' => false,
    'tableOptions' => ['class' => 'tab_perf'],
    'columns' => [
        [
            'attribute' => 'dat_nascimento',
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'headerOptions'=>['class'=>'col_acao'],
            'contentOptions' => ['class' => 'col_acao_int'],
            'header' => 'Ações',
            'viewOptions' => ['title'=>'Visualizar'],
            'deleteOptions' => ['label' => "<i class='glyphicon glyphicon-remove'></i>",
                'title'=>'Excluir',
                'msg'=>'Tem certeza que deseja excluir este perfil?'
            ],
            'buttons' => [
                //update button
                'update' => function ($url) {
                    return Html::button(null, [
                        'value' => Url::to($url),
                        'class' => 'btnModalAlterarPerfil glyphicon glyphicon-pencil',
                        'style' => 'background: transparent; border: 0; outline: none; -webkit-appearance: none; color: #337AB7;',
                        'title'=>'Editar',
                    ]);
                },
            ],
        ],
    ],
    'krajeeDialogSettings' =>[
        'options' => [
            'title' =>'Aviso',
            'btnOKLabel' =>'Sim',
            'btnCancelLabel' =>'Não',
        ],
    ],
    'pager' => [
        'options' => [
            'class' => 'pagination paginacao_sgi',
        ],
    ],
]); ?>

