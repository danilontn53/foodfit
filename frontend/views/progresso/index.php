<?php
/**
 * Created by PhpStorm.
 * User: Danilo
 * Date: 10/06/2018
 * Time: 22:22
 */

use common\components\Utils;
use dosamigos\highcharts\HighCharts;
use yii\db\Query;

$this->title = 'Meu Progresso';
$this->params['breadcrumbs'][] = $this->title
?>

<div class="text-center">

    <div class="pull-left">
        <a href="javascript:history.back()" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i>
            Voltar</a>
    </div>

    <br>
    <?php
    $user_peso = (new Query())
        ->select("*")
        ->from("user_peso")
        ->where('id = '. Yii::$app->user->identity->id)
        ->all();

    $dataByDatas = null;
    $dataByPeso = null;
    
    foreach ($user_peso as $item){
        $dataByDatas[] = date('d/m/Y', strtotime($item['dat_pesagem']));
        $dataByPeso[] = (float)$item['peso_kg'];
    }
    ?>

    <br>
    <br>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title">Evolução Peso (Kg)</h2>
            </div>
            <!--    ainda em testes-->
            <?= HighCharts::widget([
                'clientOptions' => [
                    'width' => '100%' ,
                    'chart' => [
                        'type' => 'column' ,
                    ] ,
                    'title' => [
                        'text' => 'Evolução Peso (Kg)'
                    ] ,
                    'tooltip' => [
                        'pointFormat' => '{series.name}: <b>{point.y:.0f}</b>'
                    ] ,
                    'xAxis' => [
                        'labels' => [
                            'title' => 'kcal' ,

                            'useHTML' => true ,
                            'rotation' => 0 ,
                            'style' => [
                                'fontSize' => '13px' ,
                                'fontFamily' => 'Verdana, sans-serif' ,
                                'cursor' => 'pointer'
                            ] ,
                            'formatter' => ''
                        ] ,
                        'categories' => $dataByDatas ,
                    ] ,
                    'yAxis' => [
                        'min' => 0 ,
                        'title' => 'kcal'
                    ] ,
                    'legend' => [
                        'enabled' => false ,
                    ] ,
                    'plotOptions' => [
                        'series' => [
                            'borderWidth' => 0 ,
                            'dataLabels' => [
                                'enabled' => true ,
                                'format' => '{point.y:.0f}'
                            ]
                        ] ,
                        'column' => [
                            'pointPadding' => 0.2 ,
                            'borderWidth' => 0
                        ]
                    ] ,
                    'series' => [
                        ['name' => 'Peso no período (Kg)' , 'data' => $dataByPeso] ,
                    ] ,
                    'credits' => [
                        'enabled' => false
                    ] ,
                    'lang' => [
                        'months' => ['Janeiro' , 'Fevereiro' , 'Março' , 'Abril' , 'Maio' , 'Junho' , 'Julho' , 'Agosto' , 'Setembro' , 'Outubro' , 'Novembro' , 'Dezembro'] ,
                        'shortMonths' => ['Jan' , 'Fev' , 'Mar' , 'Abr' , 'Mai' , 'Jun' , 'Jul' , 'Ago' , 'Set' , 'Out' , 'Nov' , 'Dez'] ,
                        'weekdays' => ['Domingo' , 'Segunda' , 'Terça' , 'Quarta' , 'Quinta' , 'Sexta' , 'Sábado'] ,
                        'loading' => ['Atualizando o gráfico...aguarde'] ,
                        'contextButtonTitle' => 'Exportar Gráfico' ,
                        'decimalPoint' => ',' ,
                        'thousandsSep' => '.' ,
                        'downloadJPEG' => 'Download em JPEG' ,
                        'downloadPDF' => 'Download em PDF' ,
                        'downloadPNG' => 'Download em PNG' ,
                        'downloadSVG' => 'Download vetor SVG' ,
                        'printChart' => 'Imprimir Gráfico' ,
                        'rangeSelectorFrom' => 'De' ,
                        'rangeSelectorTo' => 'Para' ,
                        'rangeSelectorZoom' => 'Zoom' ,
                        'resetZoom' => 'Limpar Zoom' ,
                        'resetZoomTitle' => 'Voltar Zoom para nível 1:1' ,
                    ] ,
                ]
            ]); ?>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title"><?=$title?></h2>
        </div>
        <?=
        HighCharts::widget([
            'clientOptions' => [
                'chart' => [
                    'type' => 'bar'
                ] ,
                'title' => [
                    'text' => $title
                ] ,
                'xAxis' => [
                    'categories' => $descricao,
                    'labels' => [
                        'title' => 'kcal' ,

                        'useHTML' => true ,
                        'rotation' => -45 ,
                        'style' => [
                            'fontSize' => '13px' ,
                            'fontFamily' => 'Verdana, sans-serif' ,
                            'cursor' => 'pointer'
                        ] ,
                        'formatter' => ''
                    ] ,
                ] ,
                'yAxis' => [
                    'title' => [
                        'text' => 'kcal'
                    ]
                ] ,
                'series' => [
                    ['name' => 'Calorias (kcal) ingeridas' , 'data' => $kcal_ingeridas] ,
                    ['name' => 'Calorias (kcal) indicadas' , 'data' => $kcal_sugeridas]
                ] ,
                'credits' => ['enabled' => false] ,
                'lang' => [
                    'months' => ['Janeiro' , 'Fevereiro' , 'Março' , 'Abril' , 'Maio' , 'Junho' , 'Julho' , 'Agosto' , 'Setembro' , 'Outubro' , 'Novembro' , 'Dezembro'] ,
                    'shortMonths' => ['Jan' , 'Fev' , 'Mar' , 'Abr' , 'Mai' , 'Jun' , 'Jul' , 'Ago' , 'Set' , 'Out' , 'Nov' , 'Dez'] ,
                    'weekdays' => ['Domingo' , 'Segunda' , 'Terça' , 'Quarta' , 'Quinta' , 'Sexta' , 'Sábado'] ,
                    'loading' => ['Atualizando o gráfico...aguarde'] ,
                    'contextButtonTitle' => 'Exportar Gráfico' ,
                    'decimalPoint' => ',' ,
                    'thousandsSep' => '.' ,
                    'downloadJPEG' => 'Download em JPEG' ,
                    'downloadPDF' => 'Download em PDF' ,
                    'downloadPNG' => 'Download em PNG' ,
                    'downloadSVG' => 'Download vetor SVG' ,
                    'printChart' => 'Imprimir Gráfico' ,
                    'rangeSelectorFrom' => 'De' ,
                    'rangeSelectorTo' => 'Para' ,
                    'rangeSelectorZoom' => 'Zoom' ,
                    'resetZoom' => 'Limpar Zoom' ,
                    'resetZoomTitle' => 'Voltar Zoom para nível 1:1' ,
                ] ,
            ]
        ]);
        ?>
    </div>
</div>
