<?php
$this->registerCss('
.form .form-control {
    width: 8% !important;
}');
?>

<div class="titulo">
    <h3>Atividades Físicas</h3>
    <hr>
</div>

<div class="row">
    <div>
        <?= $form->field($formulario, 'atividade_fisica_diaria')->radioList(
            array(
                'Sedentário' => 'Sedentário: Passa a maior parte do dia sentado (ex: caixa de banco, trabalho em escritório)',
                'Levemente Ativo' => 'Levemente Ativo: Passa boa parte do dia de pé (ex: professor, vendedor)',
                'Ativo' => 'Ativo: Passa boa parte do dia fazendo alguma atividade física (ex: garçom, carteiro)',
                'Bastante Ativo' => 'Bastante Ativo: Passa a maior parte do dia fazendo atividade física pesada (ex: carpinteiro, ciclista entregador)',
            ));
        ?>


        <?= $form->field($formulario, 'pratica_atividade_fisica')->checkboxList(
            array(
                'Não' => 'Não pratica nenhuma Atividade Física',
                'Musculação' => 'Musculação',
                'Croszfit' => 'Crossfit',
                'Caminhada leve' => 'Caminhada Leve',
                'Corrida' => 'Corrida',
                'Outro' => 'Outro',
            ));
        ?>

        <?php
        $formulario->frequencia_atividade_fisica = 0;
        ?>

        <div class="form">
            <?= $form->field($formulario, 'frequencia_atividade_fisica')->dropDownList(
                array(
                    0 => 0,
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4,
                    'indiferente' => '5 ou mais',
                ));
            ?>
        </div>
    </div>
</div>
