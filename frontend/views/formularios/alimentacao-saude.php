<?php
$this->registerCss('
.form .form-control {
    width: 8% !important;
}
.coluna .form-control {
    width: 37% !important;
}');
?>

<div class="titulo">
    <h3>Atividades Físicas</h3>
    <hr>
</div>

<div class="row">
    <div>
        <?php
        $formulario->qtd_alimentacao = 0;
        ?>

        <div class="form">
            <?= $form->field($formulario, 'qtd_alimentacao')->dropDownList(
                array(
                    0 => 0,
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4,
                    5 => '5 ou mais',
                ));
            ?>
        </div>

        <?= $form->field($formulario, 'principal_alimentacao')->radioList(
            array(
                'Café da Manhã' => 'Café da Manhã',
                'Almoço' => 'Almoço',
                'Jantar' => 'Jantar',
                'Lanches' => 'Lanches',
            ));
        ?>

        <div class="coluna">
            <?= $form->field($formulario, 'problema_saude')->textInput(); ?>
        </div>

    </div>
</div>
