<?php

?>

<div class="titulo">
    <h3>Meta</h3>
    <hr>
    <p>Selecione a Meta que deseja seguir:</p>
</div>

<div class="row">
    <div>
        <?= $form->field($formulario, 'idn_meta')->radioList(
            array(
                1 => 'Manter Peso',
                2 => 'Emagrecer',
                3 => 'Ganhar Peso',
            ));
        ?>
    </div>
</div>
