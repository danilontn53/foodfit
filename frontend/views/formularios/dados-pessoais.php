<?php

use kartik\file\FileInput;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->registerCss(".fotoClienteAntes file-zoom-dialog .kv-zoom-title {
    color: #fffefe;   
}");

?>

<div class="titulo">
    <h3>Dados Pessoais</h3>
    <hr>
</div>

<div class="row">
    <div class="col-lg-5">

        <div>
            <?= $form->field($formulario, 'dsc_nome') ?>

            <?= $form->field($formulario, 'email') ?>

            <?= $form->field($formulario, 'dat_nascimento')->widget(DatePicker::className(), [
                'options' => ['placeholder' => '', 'id' => 'dp1',],
                'language' => 'pt',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'language' => 'pt',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd/mm/yyyy',
                    ]
                ]
            ]) ?>

            <?= $form->field($formulario, 'peso_atual')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '99.99',
            ])?>

            <?= $form->field($formulario, 'altura_atual')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9.99',
            ])?>

            <?= $form->field($formulario, 'sexo')->radioList(
                array(
                    1 => 'Feminino',
                    2 => 'Masculino'
                ));
            ?>

            <div class="fotoClienteAntes">
                <?= $form->field($formulario, 'fotoClienteAntes')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],
                ]); ?>
            </div>

        </div>
    </div>
</div>
