<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\AlimentoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alimento-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_alimento') ?>

    <?= $form->field($model, 'id_tipo_alimento') ?>

    <?= $form->field($model, 'dsc_alimento') ?>

    <?= $form->field($model, 'porcao_quantidade') ?>

    <?= $form->field($model, 'caloria') ?>

    <?php // echo $form->field($model, 'carboidrato') ?>

    <?php // echo $form->field($model, 'proteina') ?>

    <?php // echo $form->field($model, 'lipidio') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
