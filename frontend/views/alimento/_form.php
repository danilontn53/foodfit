<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Alimento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alimento-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_tipo_alimento')->textInput() ?>

    <?= $form->field($model, 'dsc_alimento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'porcao_quantidade')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'caloria')->textInput() ?>

    <?= $form->field($model, 'carboidrato')->textInput() ?>

    <?= $form->field($model, 'proteina')->textInput() ?>

    <?= $form->field($model, 'lipidio')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
