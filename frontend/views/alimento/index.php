<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\AlimentoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alimentos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alimento-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Alimento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_alimento',
            'id_tipo_alimento',
            'dsc_alimento',
            'porcao_quantidade',
            'caloria',
            // 'carboidrato',
            // 'proteina',
            // 'lipidio',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
