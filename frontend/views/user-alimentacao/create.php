<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserAlimentacao */

$this->title = 'Inserir Refeição';
$this->params['breadcrumbs'][] = ['label' => 'Minhas Refeições', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-alimentacao-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
