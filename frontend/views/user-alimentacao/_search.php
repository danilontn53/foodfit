<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\UserAlimentacaoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-alimentacao-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_user_alimentacao') ?>

    <?= $form->field($model, 'id_tipo_alimento') ?>

    <?= $form->field($model, 'id_alimento') ?>

    <?= $form->field($model, 'idn_meta') ?>

    <?= $form->field($model, 'dat_refeicao') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
