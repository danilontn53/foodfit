<?php

use app\models\Alimento;
use app\models\Meta;
use app\models\TipoAlimento;
use common\components\Utils;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\UserAlimentacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Minhas Refeições';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-alimentacao-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="pull-left">
        <a href="javascript:history.back()" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i>
            Voltar</a>
    </div>

    <p class="text-right">
        <?= Html::button('<i class="glyphicon glyphicon-floppy-save" style="color: #fff"></i> Inserir Refeição', [
            'value' => Url::to(['create']),
            'class' => 'btn btn-primary',
            'id' => 'btnModalInserirRefeicao',
        ]) ?>
    </p>

    <?php
    Modal::begin([
        'header' => '<h4>Inserir Refeição</h4>',
        'id'     => 'modalInserirRefeicao',
        'size'   => 'model-lg',
    ]);

    echo "<div id='modalContentInserirRefeicao'></div>";

    Modal::end();
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'emptyText' => 'Nenhuma Refeição encontrada.',
        'export' => false,
        'tableOptions' => ['class' => 'tab_perf'],
        'columns' => [
            [
                'attribute' => 'idn_meta',
                'value' => function ($data) {
                    return isset($data['idn_meta']) ? Meta::find()->where("idn_meta = " . $data['idn_meta'])->one()->dsc_meta : null;
                },
                'headerOptions' => ['class' => 'col_tipcar'],
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Meta::find()->orderBy('dsc_meta')->asArray()->all(), 'idn_meta', 'dsc_meta'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => ''],
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:17%'],

            ],
            [
                'attribute' => 'id_tipo_alimento',
                'value' => function ($data) {
                    return isset($data['id_tipo_alimento']) ? TipoAlimento::find()->where("id_tipo_alimento = " . $data['id_tipo_alimento'])->one()->dsc_tipo_alimento : null;
                },
                'headerOptions' => ['class' => 'col_tipcar'],
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(TipoAlimento::find()->orderBy('dsc_tipo_alimento')->asArray()->all(), 'id_tipo_alimento', 'dsc_tipo_alimento'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => ''],
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:17%'],

            ],
            [
                'attribute' => 'id_alimento',
                'value' => function ($data) {
                    if (isset($data['id_alimento'])) {
                        $dsc_alimento = null;
                        $array = explode(',', $data['id_alimento']);
                        $count = count($array);

                        for ($i = 0; $i < $count; $i++) {
                            $dsc_alimento_db = Alimento::findOne(['id_alimento' => $array[$i]])->dsc_alimento;

                            if ($dsc_alimento == null) {
                                $dsc_alimento = $dsc_alimento_db;
                            } else {
                                if ($i != $count - 1) {
                                    $dsc_alimento = "$dsc_alimento, $dsc_alimento_db";
                                } else {
                                    $dsc_alimento = "$dsc_alimento e $dsc_alimento_db";
                                }
                            }
                        }

                        return $dsc_alimento;
                    } else {
                        return null;
                    }


                },
            ],
            [
                'attribute' => 'dat_refeicao',
                'value' => function ($data) {
                    return date('d/m/Y', strtotime($data['dat_refeicao']));
                }
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'headerOptions' => ['class' => 'col_acao'],
                'contentOptions' => ['class' => 'col_acao_int'],
                'header' => 'Ações',
                'viewOptions' => ['title' => 'Visualizar'],
                'deleteOptions' => ['label' => "<i class='glyphicon glyphicon-remove'></i>",
                    'title' => 'Excluir',
                    'msg' => 'Tem certeza que deseja excluir esta Marca?'
                ],
                'buttons' => [
                    //update button
                    'update' => function ($url) {
                        return Html::button(null, [
                            'value' => Url::to($url),
                            'class' => 'btnModalAlterarRefeicao glyphicon glyphicon-pencil',
                            'style' => 'background: transparent; border: 0; outline: none; -webkit-appearance: none; color: #337AB7;',
                            'title' => 'Editar',
                        ]);
                    },
                ],
            ],
        ],
        'krajeeDialogSettings' => [
            'options' => [
                'title' => 'Aviso',
                'btnOKLabel' => 'Sim',
                'btnCancelLabel' => 'Não',
            ],
        ],
        'pager' => [
            'options' => [
                'class' => 'pagination paginacao_sgi',
            ],
        ],
    ]); ?>
    <?php
    Modal::begin([
        'header'=> '<h4> Alterar Refeição</h4>',
        'id' => 'modalAlterarRefeicao',
    ]);

    echo "<div id='modalContentAlterarRefeicao'></div>";
    Modal::end();

    ?>
</div>

<?php $this->registerJs($this->render('@frontend/web/js/tabelas.js')); ?>
