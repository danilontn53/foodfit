<?php

use app\models\Alimento;
use app\models\Meta;
use app\models\TipoAlimento;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserAlimentacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-alimentacao-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_tipo_alimento')->widget(Select2::className(), [
        'data' => ArrayHelper::map(TipoAlimento::find()->orderBy('dsc_tipo_alimento')->asArray()->all(), 'id_tipo_alimento', 'dsc_tipo_alimento'),
        'maintainOrder' => true,
        'options' => [
            'placeholder' => '',
            'multiple' => false
        ],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'id_alimento')->widget(Select2::className(), [
        'data' => ArrayHelper::map(Alimento::find()->orderBy('dsc_alimento')->asArray()->all(), 'id_alimento', 'dsc_alimento'),
        'maintainOrder' => true,
        'options' => [
            'placeholder' => '',
            'multiple' => true
        ],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'dat_refeicao')->widget(DatePicker::className(), [
        'options' => ['placeholder' => '', 'id' => 'dp1',],
        'language' => 'pt',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'pluginOptions' => [
            'language' => 'pt',
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd/mm/yyyy',
            ]
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<i class="glyphicon glyphicon-floppy-save" style="color: #fff"></i> Inserir' : '<i class="glyphicon glyphicon-pencil" style="color: #fff"></i> Alterar', ['class' => $model->isNewRecord ? 'btn btn-primary btn_azul' : 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
