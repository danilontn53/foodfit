<?php

use app\models\TipoAlimento;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserAlimentacao */

$this->title = TipoAlimento::find()->where("id_tipo_alimento = ". $model->id_tipo_alimento)->one()->dsc_tipo_alimento .
    " (" . $model['dat_refeicao'] . ")";
$this->params['breadcrumbs'][] = ['label' => 'Minhas Refeições', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Atulizar'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-alimentacao-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
