<?php

use app\models\Alimento;
use app\models\Meta;
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\TipoAlimento;

/* @var $this yii\web\View */
/* @var $model app\models\UserAlimentacao */

$this->title = TipoAlimento::find()->where("id_tipo_alimento = ". $model->id_tipo_alimento)->one()->dsc_tipo_alimento .
    " (" . $model['dat_refeicao'] . ")";
$this->params['breadcrumbs'][] = ['label' => 'Minhas Refeições'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-alimentacao-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="pull-left">
        <a href="javascript:history.back()" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Voltar</a>
    </div>

    <p class="text-right">
        <?= Html::a('<i class="glyphicon glyphicon-pencil" style="color: #fff"></i> Atualizar', ['update', 'id' => $model->id_user_alimentacao, 'tela' => 'view'], ['class' => 'btn btn-primary']) ?>
        <?=  Html::a('<i class="glyphicon glyphicon-trash" style="color: #fff"></i> Apagar', ['delete', 'id' => $model->id_user_alimentacao], [
            'class' => 'btn btn-danger btn_verm btn_azul',
            'data' => [
                'confirm' => 'Tem certeza que deseja excluir esta Refeição?',
                'method' => 'post',
            ],
        ]); ?>
    </p>

    <div>
        <?= DetailView::widget([
            'model' => $model,
            'options' => ['class' => 'table table-striped table-bordered detail-view tab_perf_det'],
            'attributes' => [
                [
                    'attribute' => 'idn_meta',
                    'value' => function ($data) {
                        return isset($data['idn_meta']) ? Meta::find()->where("idn_meta = ". $data['idn_meta'])->one()->dsc_meta : null;
                    },
                ],
                [
                    'attribute' => 'id_tipo_alimento',
                    'value' => function ($data) {
                        return isset($data['id_tipo_alimento']) ? TipoAlimento::find()->where("id_tipo_alimento = ". $data['id_tipo_alimento'])->one()->dsc_tipo_alimento : null;
                    },
                ],
                [
                    'attribute' => 'id_alimento',
                    'value' => function ($data) {
                        if (isset($data['id_alimento'])) {
                            $dsc_alimento = null;

                            if (is_array($data['id_alimento'])){
                                $array = $data['id_alimento'];
                            } else {
                                $array = explode(',', $data['id_alimento']);
                            }

                            $count = count($array);

                            for ($i = 0; $i < $count; $i++) {
                                $dsc_alimento_db = Alimento::findOne(['id_alimento' => $array[$i]])->dsc_alimento;

                                if ($dsc_alimento == null) {
                                    $dsc_alimento = $dsc_alimento_db;
                                } else {
                                    if ($i != $count - 1) {
                                        $dsc_alimento = "$dsc_alimento, $dsc_alimento_db";
                                    } else {
                                        $dsc_alimento = "$dsc_alimento e $dsc_alimento_db";
                                    }
                                }
                            }

                            return $dsc_alimento;
                        } else {
                            return null;
                        }
                    },
                ],
                [
                    'attribute' => 'dat_refeicao',
//                    'value' => function ($data) {
//                        return date('d/m/Y', strtotime($data['dat_refeicao']));
//                    }
                ]
            ],
        ]) ?>
    </div>

</div>
