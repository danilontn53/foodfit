<?php
/**
 * Created by PhpStorm.
 * User: Danilo
 * Date: 10/06/2018
 * Time: 22:21
 */

namespace frontend\controllers;

use app\models\TipoAlimento;
use common\components\Utils;
use Yii;
use yii\db\Query;
use yii\web\Controller;
use app\models\Alimento;
use app\models\UserAlimentacao;

class ProgressoController extends Controller
{
    public function actionIndex(){

        $result = UserAlimentacao::find()->where(['id' => Yii::$app->user->identity->id])->orderBy('dat_refeicao')->all();
        $date = null;
        $descricao = null;
        $kcal_ingeridas = null;
        $kcal_sugeridas = null;
        $title = "Meu progresso";
        if ($result != null) {
            $kcal_indicadas = (new Query())
                ->select("*")
                ->from("user_meta")
                ->where('id = '. Yii::$app->user->identity->id)
                ->one();

            $kcal_indicadas = (float)$kcal_indicadas['peso_meta'];
            Utils::loga($result);

            foreach ($result as $item) {
                $descricao[] = date('d/m/Y', strtotime($item['dat_refeicao'])) . ' - ' . TipoAlimento::find()->where(['id_tipo_alimento' => $item['id_tipo_alimento']])->one()->dsc_tipo_alimento;
                $date[] = date('d/m/Y', strtotime($item['dat_refeicao']));
                $kcal_ingeridas [] = $this->countKcal($item['dat_refeicao'], $item['id_tipo_alimento']);
                $kcal_sugeridas[] = $kcal_indicadas;


//                if ($date != null) {
//
//
//
//
//                    if (!in_array($item['dat_refeicao'], $date)) {
//                        $date[] = date('d/m/Y', strtotime($item['dat_refeicao']));
//                        $kcal_ingeridas [] = $this->countKcal($item['dat_refeicao']);
//                        $kcal_sugeridas[] = $kcal_indicadas;
//
//                    } else {
//                        $kcal_ingeridas [] = $this->countKcal($item['dat_refeicao']);
//                        $kcal_sugeridas[] = $kcal_indicadas;
//                    }
//                } else {
//                    $date[] = date('d/m/Y', strtotime($item['dat_refeicao']));
//                    $kcal_ingeridas [] = $this->countKcal($item['dat_refeicao']);
//                    $kcal_sugeridas[] = $kcal_indicadas;
//                }
            }
            $count = count($date);

            if ($count > 1) $title = "Meu Progresso (Refeições) dentre os dias (" . $date[0] . " a " . $date[$count - 1] . ").";
            else $title = "Meu Progresso (Refeições) dentre no dia (" . $date[0] . ").";

        }
        return $this->render('index', [
            'descricao' => $descricao,
            'date' => $date,
            'kcal_ingeridas' => $kcal_ingeridas,
            'kcal_sugeridas' => $kcal_sugeridas,
            'title' => $title,
        ]);

    }

    public function countKcal($date, $id)
    {

        $result = UserAlimentacao::find()->where(['dat_refeicao' => $date, 'id_tipo_alimento' => $id])->all();
        $totalKcal = null;

        foreach ($result as $item){

            if(strpos($item['id_alimento'], ",")){
                $aux = explode(',', $item['id_alimento']);
                foreach ($aux as $id_alimento){
                    $alimento_consummido = Alimento::find()->where(['id_alimento' => $id_alimento])->one()->caloria;
                    $totalKcal += $alimento_consummido;
                }
            } else {
                $alimento_consummido = Alimento::find()->where(['id_alimento' => $item['id_alimento']])->one()->caloria;
                $totalKcal += $alimento_consummido;
            }
        }

        return $totalKcal;
    }

}