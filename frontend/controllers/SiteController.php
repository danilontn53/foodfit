<?php
namespace frontend\controllers;

use app\models\Avaliacao;
use app\models\UserAlimentacao;
use app\models\UserMeta;
use app\models\UserPeso;
use common\components\Utils;
use frontend\models\FormularioModel;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dateObjeto = null;
        $model = null;

        if (!Yii::$app->user->isGuest) {

            $kcal_indicadas = (new Query())
                ->select("*")
                ->from("user_meta")
                ->where('id = '. Yii::$app->user->identity->id)
                ->one();

            $kcal_indicadas = $kcal_indicadas['peso_meta'];

            $result = UserAlimentacao::find()->where(['id' => Yii::$app->user->identity->id, 'dat_refeicao' => date('Y-m-d')])->one();
            if ($result != null) {
                $dateObjeto[] = ['name' => 'Calorias Ingeridas neste dia: ' . date('d/m/Y', strtotime($result['dat_refeicao'])), 'y' => Utils::countKcal($result['dat_refeicao'])];
                $dateObjeto[] = ['name' => 'Calorias Indicadas para este dia', 'y' => (float)$kcal_indicadas];

            }
            $model = Avaliacao::findOne(['id' => Yii::$app->user->identity->id]);
        }

        return $this->render('index', [
            'dateObjeto' => $dateObjeto,
            'model' => $model
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(array ('site/avaliacao'));
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            $result = (new \yii\db\Query())
                ->select('*')
                ->from('avaliacao')
                ->where(['id' => Yii::$app->user->identity->id])
                ->all();

            if(!empty($result)){
                Yii::$app->getSession()->setFlash('success', [
                    'type' => 'success',
                    'duration' => 5000,
                    'icon' => 'fa fa-users',
                    'message' => 'Login realizado com sucesso!',
                    'title' => Yii::$app->user->identity->username,
                    'positonY' => 'top',
                    'positonX' => 'center'
                ]);
                return $this->redirect(array ('site/index'));

            } else {

                Yii::$app->getSession()->setFlash('success', [
                    'type' => 'success',
                    'duration' => 10000,
                    'icon' => 'fa fa-users',
                    'message' => "Login realizado com sucesso!<br>Agora é simples, responda as seguintes questões:",
                    'title' => Yii::$app->user->identity->username,
                    'positonY' => 'top',
                    'positonX' => 'center'
                ]);
                return $this->redirect(array ('site/avaliacao'));
            }

        } else {
            $model->password = '';

            if($model->load(Yii::$app->request->post())){
                Yii::$app->getSession()->setFlash('danger', [
                    'type' => 'danger',
                    'duration' => 8000,
                    'icon' => 'fa fa-window-close',
                    'message' => 'E-mail ou senha inválidos. Insira os dados corretos e tente novamente!',
                    'title' => Yii::t('app', Html::encode('Erro ao realizar login')),
                    'positonY' => 'top',
                    'positonX' => 'center'
                ]);

                return $this->render('index');
            } else {
                return $this->renderAjax('login', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays Avaliacao page.
     *
     * @return mixed
     */
    public function actionAvaliacao()
    {
        $model = new Avaliacao();

        if (!empty(Yii::$app->user->identity->dat_nascimento)){
            $date = date_create(Yii::$app->user->identity->dat_nascimento);
        } else {
            $date = null;
        }

        $model['dsc_nome'] = Yii::$app->user->identity->username;
        $model['email'] = Yii::$app->user->identity->email;
        $model['dat_nascimento'] = date_format($date, 'd/m/Y');

        if($model->load(Yii::$app->request->post())) {

            (new Query())->createCommand()->update("user", [
                "username" => $model->dsc_nome,
                "email" => $model->email,
                "dat_nascimento" => $model->dat_nascimento,
            ], 'id = ' . Yii::$app->user->identity->id)->execute();

            $model->id = Yii::$app->user->identity->id;
            $model->fotoClienteAntes = UploadedFile::getInstance($model, 'fotoClienteAntes');

            $model->peso_atual = $model->peso_atual;
            $model->foto_antes = $model->fotoClienteAntes->name;

            //save avaliacao
            (new Query())->createCommand()->insert("avaliacao", [
                "id" => $model->id,
                "foto_antes" => $model->foto_antes,
                "peso_atual" => $model->peso_atual,
                "altura_atual" => $model->altura_atual,
                "sexo" => $model->sexo,
                "idn_meta" => $model->idn_meta,
            ])->execute();

            //save meta
            $user_meta = new UserMeta();
            $user_meta->id = Yii::$app->user->identity->id;
            $user_meta->idn_meta = $model->idn_meta;

            //save meta
            $user_peso = new UserPeso();
            $user_peso->id = Yii::$app->user->identity->id;
            $user_peso->peso_kg = $model->peso_atual;
            $user_peso->dat_pesagem = date('d/m/Y');
            $user_peso->save();


            if ($model->idn_meta == 2) {
                $kcal = -300;
            } elseif ($model->idn_meta == 3) {
                $kcal = 300;
            } else {
                $kcal = 0;
            }

                if ($model->sexo == 1) {
                $user_meta->peso_meta = $kcal + (66.5 + (9.6 * $model->peso_atual) + (1.9 * ($model->altura_atual * 100) - (4.7 * $this->calculaIdade($model->dat_nascimento))));
            } else {
                $user_meta->peso_meta = $kcal + (66.5 + (13.7 * $model->peso_atual) + (5 * ($model->altura_atual * 100) - (6.8 * self::calculaIdade($model->dat_nascimento))));
            }

            $user_meta->save();

            $uploadPathAntes = Yii::getAlias('@webroot/files/antes');
            $model->fotoClienteAntes->saveAs($uploadPathAntes. '/' . $model->fotoClienteAntes->name);

            Yii::$app->getSession()->setFlash('success', [
                'type' => 'success',
                'duration' => 10000,
                'icon' => 'fa fa-users',
                'message' => "Acompanhe agora o seu progresso!",
                'title' => "A sua Avaliação foi salva com sucesso",
                'positonY' => 'top',
                'positonX' => 'center'
            ]);
            return $this->redirect(array ('site/index'));
        }

        return $this->render('avaliacao', [
            'formulario' => $model
        ]);
    }

    public static function calculaIdade($data) {
        // Separa em dia, mês e ano
        list($dia, $mes, $ano) = explode('/', $data);

        // Descobre que dia é hoje e retorna a unix timestamp
        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        // Descobre a unix timestamp da data de nascimento do fulano
        $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);

        // Depois apenas fazemos o cálculo já citado :)
        $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

        return $idade;
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {

                    Yii::$app->getSession()->setFlash('success', [
                        'type' => 'success',
                        'duration' => 10000,
                        'icon' => 'fa fa-users',
                        'message' => "Cadastro realizado com sucesso!<br>Agora é simples, responda as seguintes questões:",
                        'title' => Yii::$app->user->identity->username,
                        'positonY' => 'top',
                        'positonX' => 'center'
                    ]);
                    return $this->redirect(array ('site/avaliacao'));
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionSugestaoRefeicao()
    {

        return $this->render('sugestao-refeicao');
    }
}
