<?php

namespace frontend\controllers;

use app\models\UserMeta;
use common\components\Utils;
use Yii;
use app\models\UserAlimentacao;
use frontend\models\UserAlimentacaoSearch;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserAlimentacaoController implements the CRUD actions for UserAlimentacao model.
 */
class UserAlimentacaoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserAlimentacao models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserAlimentacaoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserAlimentacao model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserAlimentacao model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserAlimentacao();

        if ($model->load(Yii::$app->request->post())) {

            $model->id = Yii::$app->user->identity->id;
            $model->id_alimento = implode(',', $model['id_alimento']);
            $model->idn_meta = UserMeta::find()->where(['id' => Yii::$app->user->identity->id])->one()->idn_meta;
            $model->save();

            return $this->redirect(['view', 'id' => $model->id_user_alimentacao]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UserAlimentacao model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $tela = null)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->id_alimento = implode(',', $model['id_alimento']);
            $model->save();

            return $this->redirect(['view', 'id' => $model->id_user_alimentacao]);
        }

        if ($tela == 'view'){
            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserAlimentacao model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserAlimentacao model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserAlimentacao the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserAlimentacao::findOne($id)) !== null) {

            $model->id_alimento = explode(',', $model->id_alimento);
            $model->dat_refeicao = date('d/m/Y', strtotime($model->dat_refeicao));

            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
