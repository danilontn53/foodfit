<?php
/**
 * Created by PhpStorm.
 * User: 03520562154
 * Date: 09/08/2016
 * Time: 11:58
 */

namespace common\components;


class Request extends \yii\web\Request
{
    public $web;
    public $adminUrl;

    public function getBaseUrl(){
        return str_replace($this->web, "", parent::getBaseUrl()) . $this->adminUrl;
    }

    public function resolvePathInfo(){
        if($this->getUrl() === $this->adminUrl){
            return "";
        }else{
            return parent::resolvePathInfo();
        }
    }
}