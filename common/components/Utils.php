<?php
/**
 * Created by PhpStorm.
 * User: 03520562154
 * Date: 27/10/2016
 * Time: 11:29
 * Classe criada para manter funções que poderão serem utilizadas em qualquer parte do projeto
 */

namespace common\components;

use app\models\Alimento;
use app\models\UserAlimentacao;
use Swift_Image;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class Utils
{
    /** Escreve no arquivo de log de erros do PHP */
    public static function loga($msg)
    {
        if(!empty($msg)) {
            error_log(print_r($msg,true));
        }
    }

    public static function countKcal($date)
    {

        $result = UserAlimentacao::find()->where(['dat_refeicao' => $date])->all();
        $totalKcal = null;

        foreach ($result as $item) {

            if (strpos($item['id_alimento'], ",")) {
                $aux = explode(',', $item['id_alimento']);
                foreach ($aux as $id_alimento) {
                    $alimento_consummido = Alimento::find()->where(['id_alimento' => $id_alimento])->one()->caloria;
                    $totalKcal += $alimento_consummido;
                }
            } else {
                $alimento_consummido = Alimento::find()->where(['id_alimento' => $item['id_alimento']])->one()->caloria;
                $totalKcal += $alimento_consummido;
            }
        }

        return $totalKcal;
    }

    /** Remove acentos */
    public static function removeAcentos($msg) {
        $a = array(
            "/[ÂÀÁÄÃ]/u"=>"A",
            "/[âãàáä]/u"=>"a",
            "/[ÊÈÉË]/u"=>"E",
            "/[êèéë]/u"=>"e",
            "/[ÎÍÌÏ]/u"=>"I",
            "/[îíìï]/u"=>"i",
            "/[ÔÕÒÓÖ]/u"=>"O",
            "/[ôõòóö]/u"=>"o",
            "/[ÛÙÚÜ]/u"=>"U",
            "/[ûúùü]/u"=>"u",
            "/ç/u"=>"c",
            "/Ç/u"=> "C"
        );
        return preg_replace(array_keys($a), array_values($a), $msg);
    }

    /** Retorna a letra da coluna a partir do índice. 1 = A, 2 = B */
    public static function getNameFromIndex($index) {
        $numeric = ($index - 1) % 26;
        $letter = chr(65 + $numeric);
        $index2 = intval(($index - 1) / 26);
        if ($index2 > 0) {
            return self::getNameFromIndex($index2) . $letter;
        } else {
            return $letter;
        }
    }

    public static function getMimeType($file) {
        $idx = explode( '.', $file );
        $count_explode = count($idx);
        $idx = strtolower($idx[$count_explode-1]);

        $mimet = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',


            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        if (isset( $mimet[$idx] )) {
            return $mimet[$idx];
        } else {
            return 'application/octet-stream';
        }
    }

    public static function enviaEmail($remetente, $destinatario, $assunto, $mensagem, $nome, $pathImageEmbed = "") {
        try {
            $message = Swift_Message::newInstance()
                ->setSubject($assunto)
                ->setFrom([$remetente => $nome])
                ->setTo($destinatario)
                ->setCharset('utf-8')
                ->setContentType('text/html')
            ;

            if(!empty($pathImageEmbed)) {
                $cid = $message->embed(Swift_Image::fromPath($pathImageEmbed));
                $mensagem .= '<img src="'.$cid.'" alt="imagem"/>';
            }

            $message->setBody($mensagem);

            $transport = Swift_SmtpTransport::newInstance('10.209.253.28', 25);
            $mailer = Swift_Mailer::newInstance($transport);

            $result = $mailer->send($message);

            return $result;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function removeAcentosECaracteresEspeciais($str) {
        $str = preg_replace('/[áàãâä]/ui', 'a', $str);
        $str = preg_replace('/[éèêë]/ui', 'e', $str);
        $str = preg_replace('/[íìîï]/ui', 'i', $str);
        $str = preg_replace('/[óòõôö]/ui', 'o', $str);
        $str = preg_replace('/[úùûü]/ui', 'u', $str);
        $str = preg_replace('/[ç]/ui', 'c', $str);
        //$str = preg_replace('/[,(),;:|!"#$%&/=?~^><ªº-]/', '_', $str);
        $str = preg_replace('/[^a-z0-9]/i', '', $str);
        //$str = preg_replace('/_+/', '', $str); // ideia do Bacco :)
        return $str;
    }

    public static function dateMascara ($str)
    {
        $contBarra = 0;
        $formatoErrado = true;
        $t = str_replace(0, 'a', $str);

        // Verifica se a quantidade de barras na data é 2
        for ($i = 0; $i < strlen($t); $i++) {
            if (strcasecmp($t[$i], "/") == 0) {
                $contBarra++;
            }
        }
        if ($contBarra == 2) {
            $mask = "##/##/####";//mascara formato data

            if (strpos($str, "/")) {

                $str = explode("/", str_replace(" ", "", $str));
                $strDia = $str[0];//dia
                $strMes = $str[1];//mês
                $strAno = $str[2];//ano

                /* =======> Inicio da verificação se o dia, mês e ano estão no formato correto<======= */
                if ((strlen($strDia) == 2 || strlen($strDia) == 1) && (strlen($strMes) == 2 || strlen($strMes) == 1) && (strlen($strAno) == 4 || strlen($strAno) == 2)) {
                    $formatoErrado = false;
                }

                /* =======> entrada de data: 1/1/07 <======= */
                if ($formatoErrado == false) {

                    if (strlen($strDia) == 1) {//altera o dia ex: de 1 para 01
                        $str[0] = "0$strDia";
                    }
                    if (strlen($strMes) == 1) {//altera o mês ex: de 1 para 01
                        $str[1] = "0$strMes";
                    }
                    if (strlen($strAno) == 2) {//altera o ano ex: de 07 para 2007
                        $str[2] = "20$strAno";
                    }
                    /* =======> saída de data: 01/01/2007 <======= */

                    //monta o array '$str' em uma string '$stringStr'
                    $stringStr = null;
                    foreach ($str as $stringArray) {
                        $stringStr = $stringStr . $stringArray;
                    }

                    if (strlen($mask) == (strlen($stringStr) + 2)) {
                        //monta formato da mascara da data
                        for ($i = 0; $i < strlen($stringStr); $i++) {
                            $mask[strpos($mask, "#")] = $stringStr[$i];
                        }
                    }
                    return $mask;
                }
            }
        }
    }

    /**
     * Ordena um array
     * @param $array
     * @param $on
     * @param int $order
     * @return array
     */
    public static function array_sort($array, $on, $order=SORT_ASC)
    {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }
}