<?php

namespace common\components;
use yii\base\Widget;

class ShowAnoCorrenteWidget extends Widget
{
    public $anoCorrente = '';
    public $tipoValor = '';

    public function run()
    {
        $html = "<div class='panel panel-default' style=''>
                <div class='panel-heading'>
                    <h2 class='panel-title' style='float: none; min-height: 30px;'> " . $this->anoCorrente . " (".$this->tipoValor. ")" . "</h2>
                </div>
            </div>";

        echo $html;
    }
}