<?php

namespace common\components;

use yii\base\Widget;
use yii\bootstrap\Html;

class ButtonBackWidget extends Widget
{
    public $width = '100px';

    public function run()
    {
        echo Html::button('<i class="glyphicon glyphicon-arrow-left"></i> Voltar', [
            'name' => 'btnBack',
            //'style' => 'width:'.$this->width.';',
            'class' => 'btn btn-success',
            'onclick' => "history.go(-1)",
        ]);
    }
}