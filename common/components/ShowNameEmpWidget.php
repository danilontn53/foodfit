<?php

namespace common\components;

use yii\base\Widget;

class ShowNameEmpWidget extends Widget
{
    public $idnEmpreendimento = '';
    public $dscTitulo = '';

    public function run()
    {
        $html = "<div class='panel panel-default' style=''>
                <div class='panel-heading'>
                    <h2 class='panel-title' style='float: none; min-height: 30px;'>Empreendimento " . $this->dscTitulo . " (".$this->idnEmpreendimento. ")" . "</h2>
                </div>
            </div>";
        
        echo $html;
    }
}